DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`
(
    `id`          bigint(20) NOT NULL COMMENT '用户ID',
    `id_card`     varchar(18)                                     default NULL COMMENT '用户身份证ID ',
    `user_acct`   varchar(30) character set utf8 collate utf8_bin default NULL COMMENT ' 用户账号 ',
    `user_name`   varchar(100)                                    default NULL COMMENT ' 用户姓名 ',
    `user_pinyin` varchar(225)                                    default NULL COMMENT ' 拼音 ' ,
    `user_mobnum` varchar(11)                                     default NULL COMMENT ' 手机号码 ',
    `phone_sms`   varchar(20)                                     default NULL COMMENT ' 短信通知号码 ',
    `user_email`  varchar(50)                                     default NULL COMMENT ' 邮箱 ',
    `login_pw`    varchar(500)                                    default NULL COMMENT ' 密码 ',
    `pw_salt`     varchar(64)                                     default NULL COMMENT ' 密码盐 ',
    `user_age`    int(11)                                         default NULL COMMENT ' 用户年龄 ',
    `user_gen`    varchar(20)                                     default NULL COMMENT ' 用户性别 ',
    `user_sts`    varchar(20)                                     default NULL COMMENT ' 用户状态 ',
    `user_addr`   varchar(500)                                    default NULL COMMENT ' 用户地址 ',
    `user_telnum` varchar(20)                                     default NULL COMMENT ' 用户座机 ',
    `rec_flag`    char(1)                                         default NULL COMMENT ' 记录标志(0删除;1有效;)',
    `created_by` bigint(20)                                       default NULL COMMENT '创建用户ID',
    `created_date` datetime                                       default NULL COMMENT '创建时间',
    `modified_by` bigint(20)                                      default NULL COMMENT '修改用户ID',
    `modified_date` datetime                                      default NULL COMMENT '修改时间',
    `user_type` varchar(20)                                       default '0' COMMENT '用户类型',
    `remark` varchar(4000)                                        default NULL,
     PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';

DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(100) NOT NULL COMMENT '角色名称',
  `role_desc` varchar(500) DEFAULT NULL COMMENT '角色描述',
  `role_type` varchar(20) NOT NULL COMMENT '角色类型：100:ADMIN角色,200:子管理员角色,400:普通角色',
  `role_flag` varchar(20) NOT NULL COMMENT '是否内置角色 1内置角色 0非内置角色',
  `rec_flag` char(1) DEFAULT NULL COMMENT '1有效，0无效',
  `created_by` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `created_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '最后修改用户',
  `modified_date` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8 COMMENT='角色表';

DROP TABLE IF EXISTS `sys_rs`;
CREATE TABLE `sys_rs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '资源ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  `rs_name` varchar(100) NOT NULL COMMENT '资源名称',
  `rs_url` varchar(500) NOT NULL COMMENT '资源URL',
  `rs_view` varchar(255) DEFAULT NULL,
  `rs_cls` varchar(500) DEFAULT NULL COMMENT '资源样式',
  `rs_ord` int(11) NOT NULL COMMENT '资源顺序',
  `rs_type` varchar(20) NOT NULL COMMENT '资源类型',
  `rs_desc` varchar(500) DEFAULT NULL COMMENT '资源描述',
  `rec_flag` char(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL COMMENT '创建用户',
  `created_by` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `modified_date` datetime DEFAULT NULL COMMENT '最后修改用户',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=637 DEFAULT CHARSET=utf8 COMMENT='资源表';

DROP TABLE IF EXISTS `sys_auth`;
CREATE TABLE `sys_auth` (
  `auth_id` bigint(20) NOT NULL COMMENT '控制者ID',
  `auth_type` varchar(20) NOT NULL COMMENT '用户:100,角色:200,岗位:400,菜单:300,资源:500',
  `auth_obj_id` bigint(20) NOT NULL COMMENT '控制对象ID',
  `auth_obj_type` varchar(20) NOT NULL COMMENT '菜单:300,资源:500',
  `created_by` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `created_date` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限表';

DROP TABLE IF EXISTS `sys_auth_temp`;
CREATE TABLE `sys_auth_temp` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `user_type` varchar(20) NOT NULL COMMENT '用户类型',
  `auth_id` bigint(20) NOT NULL COMMENT '控制者ID',
  `auth_type` varchar(20) NOT NULL COMMENT '控制者类型',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `role_type` varchar(20) NOT NULL COMMENT '角色类型',
  `rs_id` bigint(20) NOT NULL COMMENT '菜单ID, 资源ID',
  `rs_type` varchar(20) NOT NULL COMMENT '菜单:300,资源:500',
  `created_by` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `created_date` datetime DEFAULT NULL COMMENT '创建时间',
  KEY `idx_cf_sys_priv_temp_01` (`user_id`,`auth_id`,`role_id`,`rs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限临时表';

DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `ys_menu` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `parent_id` bigint(20) NOT NULL COMMENT '父菜单ID',
    `menu_name` varchar(100) NOT NULL COMMENT '菜单名称',
    `menu_url` varchar(500) DEFAULT NULL COMMENT '菜单URL',
    `menu_cls` varchar(500) DEFAULT NULL COMMENT '菜单样式',
    `menu_ord` int(11) NOT NULL COMMENT '菜单顺序',
    `menu_type` varchar(20) NOT NULL COMMENT '菜单类型',
    `menu_lvl` int(11) NOT NULL COMMENT '菜单层级',
    `menu_desc` varchar(500) DEFAULT NULL COMMENT '菜单描述',
    `rec_flag` char(1) DEFAULT NULL,
    `created_by` int(11) DEFAULT NULL COMMENT '创建用户',
    `created_date` datetime DEFAULT NULL COMMENT '创建时间',
    `modified_by` int(11) DEFAULT NULL COMMENT '最后修改用户',
    `modified_date` datetime DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=384 DEFAULT CHARSET=utf8 COMMENT='菜单表';
