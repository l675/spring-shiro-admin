package com.fanfan.shiroadmin.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * 自定义redis配置信息
 * @author DunFan.Liao
 * @date 2021/8/19 13:48
 */
@Configuration
public class RedisConfig
{

    @Autowired
    RedisConnectionFactory factory;

    @Bean
    public RedisTemplate<String,Object> redisTemplate()
    {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new StringRedisSerializer());
        redisTemplate.setConnectionFactory(factory);
        return redisTemplate;
    }

    /**
    * 功能描述：通过ValueOperations设置redis值
    * @param redisTemplate
    * @return
    * @author DunFan.Liao
    * @date 2021/8/19 13:56
    */
    @Bean
    public ValueOperations<String, String> opsForValue(RedisTemplate<String, String> redisTemplate)
    {
        return redisTemplate.opsForValue();
    }
//
//    @Bean
//    public SetOperations<String, Object> opsForSet(RedisTemplate<String, Object> redisTemplate){
//        return redisTemplate.opsForSet();
//    }



}
