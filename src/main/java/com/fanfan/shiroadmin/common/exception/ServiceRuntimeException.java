package com.fanfan.shiroadmin.common.exception;

/**
 * 自定义RuntimeException异常
 * @author DunFan.Liao
 * @date 2021/8/19 18:06
 */
public class ServiceRuntimeException extends RuntimeException
{
    private String msg;
    private int code = 500;

    public ServiceRuntimeException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public ServiceRuntimeException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public ServiceRuntimeException(String msg, int code) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public ServiceRuntimeException(String msg, int code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
