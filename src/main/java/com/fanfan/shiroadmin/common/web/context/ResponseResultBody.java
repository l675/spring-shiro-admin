package com.fanfan.shiroadmin.common.web.context;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author DunFan.Liao
 * @Date 2021/8/17 14:27
 */
public class ResponseResultBody implements IResponseResultBody
{

    private static final long serialVersionUID = 1701152596751319875L;
    /**
     * 执行结果状态
     */
    private boolean status;
    /**
     * 错误消息集合
     */
    private List<String> message;

    /**
     * 执行成功返回JSON对象
     */
    private JSONObject result;

    /**
     * 状态码
     */
    private int code;

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public ResponseResultBody(boolean status, JSONObject result)
    {
        this.status = status;
        this.result = result;
        this.message = new ArrayList<>();
    }

    public ResponseResultBody(boolean status, JSONObject result, int code)
    {
        this.status = status;
        this.result = result;
        this.code = code;
        this.message = new ArrayList<>();
    }

    @Override
    public boolean isSuccess()
    {
        return this.status;
    }

    @Override
    public void setStatus(boolean status)
    {
        this.status = status;

    }

    @Override
    public String getMessage()
    {
        StringBuilder builder = new StringBuilder();
        for (int index = 0,size = this.message.size(); index < size; index++)
        {
            String message = this.message.get(index);
            builder.append(message);
            if (index < size + 1)
            {
//                换行
                builder.append("\r\n");
            }
        }
        return builder.toString();
    }

    @Override
    public void addMessage(String message)
    {
        if (null == message)
        {
            this.message = new ArrayList<String>();
        }
        this.message.add(message);
    }

    @Override
    public JSONObject getResult()
    {
        return this.result;
    }

    @Override
    public void setResult(JSONObject result)
    {
        this.result = result;
    }

    public static JSONObject createFailedResult()
    {
        IResponseResultBody result = new ResponseResultBody(false,null);
        result.addMessage("请求失败！");
        return (JSONObject) JSONObject.toJSON(result);
    }

    public static JSONObject createFailedResult(String message)
    {
        IResponseResultBody result = new ResponseResultBody(false,null);
        result.addMessage(message);
        return (JSONObject) JSONObject.toJSON(result);
    }
    public static JSONObject createFailedResult(int code,String message)
    {
        IResponseResultBody result = new ResponseResultBody(false,null,code);
        result.addMessage(message);
        return (JSONObject) JSONObject.toJSON(result);
    }

    public static JSONObject createSuccessResult()
    {
        IResponseResultBody result = new ResponseResultBody(true, null);
        result.addMessage("请求成功！");
        return (JSONObject) JSONObject.toJSON(result);
    }
    public static JSONObject createSuccessResult(List<? extends Object> data)
    {
        JSONObject json = new JSONObject();
        json.put("data", new JSONArray(new ArrayList<Object>(data)));
        json.put("total",data.size());
        IResponseResultBody result = new ResponseResultBody(true,json);
        result.addMessage("请求成功！");
        return (JSONObject) JSONObject.toJSON(result);
    }
    public static JSONObject createSuccessResult(Object object)
    {
        JSONObject json = (JSONObject) JSONObject.toJSON(object);
        IResponseResultBody result = new ResponseResultBody(true,json);
        result.addMessage("请求成功！");
        return (JSONObject) JSONObject.toJSON(result);
    }
}
