package com.fanfan.shiroadmin.common.web.entiy;

import com.alibaba.fastjson.annotation.JSONField;
import com.fanfan.shiroadmin.common.utils.Constant;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体模型的基类
 * @author DunFan.Liao
 * @date 2021/8/17 16:03
 */
public abstract class BaseEntity implements Serializable
{
    private static final long serialVersionUID = 5050738369272690115L;


    /** 应用编码*/
    private String appCode;

    /** 当前记录的版本号 */
    private Integer version;

    /** 记录标志*/
    private String recFlag;

    /** 创建的时间*/
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;

    /** 修改的时间 */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date modifiedDate;

    public BaseEntity()
    {
        this.setRecFlag("1");
        this.setVersion(1);
    }

    public String getAppCode()
    {
        return appCode;
    }

    public void setAppCode(String appCode)
    {
        this.appCode = appCode;
    }

    public Integer getVersion()
    {
        return version;
    }

    public void setVersion(Integer version)
    {
        this.version = version;
    }

    public String getRecFlag()
    {
        return recFlag;
    }

    public void setRecFlag(String recFlag)
    {
        this.recFlag = recFlag;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate()
    {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

}
