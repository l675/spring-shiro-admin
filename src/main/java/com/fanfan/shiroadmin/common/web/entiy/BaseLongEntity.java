package com.fanfan.shiroadmin.common.web.entiy;

/**
 * 主键为Long类型-实体模型的基类
 * @author DunFan.Liao
 * @date 2021/9/28 16:51
 */
public class BaseLongEntity extends BaseEntity
{
    /** 主键 */
    private long id;

    /** 创建的人-主键 */
    private long createdBy;

    /** 修改的人-主键 */
    private long modifiedBy;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public long getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(long createdBy)
    {
        this.createdBy = createdBy;
    }

    public long getModifiedBy()
    {
        return modifiedBy;
    }

    public void setModifiedBy(long modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }
}
