package com.fanfan.shiroadmin.common.web.context;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;

/**
 * @author DunFan.Liao
 * @date 2021/8/17 13:53
 */
public interface IResponseResultBody extends Serializable
{
    /**
     * 功能描述：是否成功
     * @return 成功返回true 失败返回false
     * @author DunFan.Liao
     * @date 2021/8/17
     */
    public boolean isSuccess();
    /**
    * 功能描述：
    * @param status 执行成功设置为true, 否则false.
    * @author DunFan.Liao
    * @date 2021/8/17
    */
    public void setStatus(boolean status);

    /**
    * 功能描述：获取消息
    * @return 错误的消息
    * @author DunFan.Liao
    * @date 2021/8/17
    */
    public String getMessage();
    /**
    * 功能描述：添加错误消息
    * @param message 错误信息
    * @author DunFan.Liao
    * @date 2021/8/17
    */
    public void addMessage(String message);

    /**
    * 功能描述：获取执行成功的JSON对象
    * @return com.alibaba.fastjson.JSONObject
    * @author DunFan.Liao
    * @date 2021/8/17
    */
    public JSONObject getResult();

    /**
    * 功能描述：设置执行成功的返回JSON对象
    * @param result 执行成功JSON对象
    * @author DunFan.Liao
    * @date 2021/8/17 14:22
    */
    public void setResult(JSONObject result);


}
