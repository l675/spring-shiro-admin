package com.fanfan.shiroadmin.common.web.entiy;

import java.io.Serializable;

/**
 * 系统登录用户接口
 * @author DunFan.Liao
 * @date 2021/9/28 16:45
 */
public interface ISubjectUser extends Serializable
{
    /**
     * 获得当前登录的用ID
     *
     * @return 当前登录用ID
     */
    public Long getLoginUserId();

    /**
     * 获得当前登录用户账号
     *
     * @return 当前登录用户账号
     */
    public String getLoginUserAccount();

    /**
     * 获得当前登录用户名
     *
     * @return 当前登录用户名
     */
    public String getLoginUserName();

    /**
     * 获得当前登录用户是否为超级管理员
     *
     * @return 当前登录用户是超级管理员返回true, 否则false
     */
    public boolean isSuperAdmin();

    /**
     * 获得用户当前登录的系统编码
     *
     * @return 用户当前登录的系统编码
     */
    public String getCurrentAppCode();
}
