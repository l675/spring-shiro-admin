package com.fanfan.shiroadmin.common.web.controller;

import com.fanfan.shiroadmin.common.web.entiy.SubjectUser;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller公共组件
 *
 * @author DunFan.Liao
 * @date 2021-8-19 16:01
 */
public abstract class AbstractController
{
	protected Logger logger = LoggerFactory.getLogger(getClass());

	protected SubjectUser getUser()
	{
		return (SubjectUser) SecurityUtils.getSubject().getPrincipal();
	}

	protected Long getUserId()
	{
		return getUser().getId();
	}
}
