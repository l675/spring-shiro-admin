package com.fanfan.shiroadmin.common.web.entiy;

/**
 * 抽象的用户对象
 * @author DunFan.Liao
 * @date 2021/9/28 16:48
 */
public class SubjectUser extends BaseLongEntity
{

    /** 登录用户账号*/
    private String userAcct;

    /** 登录用户名称*/
    private String userName;

    public String getUserAcct()
    {
        return userAcct;
    }

    public void setUserAcct(String userAcct)
    {
        this.userAcct = userAcct;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }
}
