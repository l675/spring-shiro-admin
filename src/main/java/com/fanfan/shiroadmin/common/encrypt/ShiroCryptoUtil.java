package com.fanfan.shiroadmin.common.encrypt;

import org.apache.shiro.crypto.hash.Md5Hash;

import java.util.Random;

/**
 * 基于Shiro的加密工具类
 * @author DunFan.Liao
 * @date 2021/9/28 9:31
 */
public class ShiroCryptoUtil
{
    /**
     * MD5
     */
    private static final String MD5 = "MD5";

    /**
     * 默认hash值
     */
    private static final int DEFAULT_HASH = 1024;

    /**
     * 默认盐位数
     */
    private static final int DEFAULT_SALT_COUNT = 24;

    /**
     * 默认盐值
     */
    private static final String DEFAULT_SALT = "888888888888888888888888";

    private static String getMd5Password(String password)
    {
        return getMd5Password(password,DEFAULT_SALT);
    }
    private static String getMd5Password(String password,String salt)
    {
        return getMd5Password(password,salt,DEFAULT_HASH);
    }
    /**
     * 功能描述：获得MD5加密后的密码值
     * @param password 原始密码
     * @param salt 盐
     * @param hashValue 散列值
     * @return 加密后的MD5值
     * @author DunFan.Liao
     * @date 2021/9/28 10:01
     */
    private static String getMd5Password(String password, String salt, int hashValue)
    {
        Md5Hash md5Hash = new Md5Hash(password, salt, hashValue);
        return md5Hash.toHex();
    }

    public static void main(String[] args) {
        System.out.println(getMd5Password("123","123456987106498789",1024));
    }

}
