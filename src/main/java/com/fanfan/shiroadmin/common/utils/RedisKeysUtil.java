package com.fanfan.shiroadmin.common.utils;

import org.springframework.beans.factory.annotation.Value;

/**
 * 设置redis的key格式
 * @author DunFan.Liao
 * @date 2021/8/19 17:30
 */
public class RedisKeysUtil
{
    /**
    * 功能描述：获得redis存放验证码的key
    * @param key 自定义key
    * @return 格式化key
    * @author DunFan.Liao
    * @date 2021/8/19 17:37
    */
    public static String getCaptchaKey(String namespace,String key)
    {
        return namespace + ":captcha:"+key;
    }
    /**
     * 功能描述：获得redis存放在线用户的key
     * @param token 自定义key
     * @return 格式化key
     * @author DunFan.Liao
     * @date 2021/8/19 17:38
     */
    public static String getOnlineUserKey(String sessionNamespace,String token)
    {
        return sessionNamespace + ":online:"+ token;
    }
    public static String getTokenUserKey(String sessionNamespace,String userAcct)
    {
        return sessionNamespace + ":token:"+ userAcct;
    }

    public static String getMenuUserKey(String namespace,String userAcct)
    {
        return namespace + ":menu:"+userAcct;
    }
    public static String getRsUserKey(String namespace,String userAcct)
    {
        return namespace + ":resources:"+userAcct;
    }

}
