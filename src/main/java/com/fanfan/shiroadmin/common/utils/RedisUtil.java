package com.fanfan.shiroadmin.common.utils;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 自定义redis帮助类
 * @author DunFan.Liao
 * @date 2021/8/19 11:10
 */
@Component
public class RedisUtil
{

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private ValueOperations<String, String> valueOperations;
    /**
     * 不设置过期时长
     */
    public static final long NOT_EXPIRE = -1;


    @Value("${redis.spring.session.maxInactiveIntervalInSeconds}")
    private String expireTime;

    private static final Gson GSON = new Gson();
    public void set(String key,Object value)
    {
        set(key,value, Long.parseLong(this.expireTime));
    }

    /**
    * 功能描述：保存数据到redis缓存
    * @param key namespace+key
    * @param value 数据
    * @param expireTime 过期时间
    * @author DunFan.Liao
    * @date 2021/8/19 16:38
    */
    public void set(String key,Object value,long expireTime)
    {
        valueOperations.set(key,toJson(value));
        if (expireTime != NOT_EXPIRE)
        {
            redisTemplate.expire(key,expireTime,TimeUnit.SECONDS);
        }
    }

    public String get(String key)
    {
        return get(key,NOT_EXPIRE);
    }
    public String get(String key,long expireTime)
    {
        String value = valueOperations.get(key);
        if (expireTime != NOT_EXPIRE)
        {
            redisTemplate.expire(key,expireTime,TimeUnit.SECONDS);
        }
        return value;
    }
    /**
    * 功能描述：获取redis缓存中的对象
    * @param key redis中的命名空间+key
    * @param  clazz 需要映射的类class
    * @return 需要映射的类的实体
    * @author DunFan.Liao
    * @date 2021/8/19 16:55
    */
    public <T> T get(String key,Class<T> clazz,long expireTime)
    {
        String value = valueOperations.get(key);
        if (expireTime != NOT_EXPIRE)
        {
            redisTemplate.expire(key,expireTime,TimeUnit.SECONDS);
        }
        return value == null ? null : fromJson(value,clazz);
    }


    /**
    * 功能描述：实体对象装换json
    * @param o Object对象
    * @return json数据
    * @author DunFan.Liao
    * @date 2021/8/19 14:56
    */
     private String toJson(Object o)
     {
        if (o instanceof Integer || o instanceof Double || o instanceof Float || o instanceof Long ||
        o instanceof Boolean || o instanceof String || o instanceof Short )
        {
            // 基本数据类型调用String.valueOf(object)转换更快
            return String.valueOf(o);
        }
        else
        {
            return GSON.toJson(o);
        }
    }

    /**
    * 功能描述：JSON数据，转成Object
    * @param json json数据
    * @param clazz 具体类的class
    * @return 具体类的实体对象
    * @author DunFan.Liao
    * @date 2021/8/19 14:59
    */
    private <T> T fromJson(String json,Class<T> clazz)
    {
        return GSON.fromJson(json, clazz);
    }




}
