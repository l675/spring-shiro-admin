package com.fanfan.shiroadmin.common.utils;

/**
 * 系统常量定义
 * @author DunFan.Liao
 * @date 2021/8/19 16:17
 */
public class Constant {

    /**
     * 超级管理员账号
     */
    public static final Long SUPER_ADMIN = 1L;

    /**
     * 当前页面
     */
    public static final String PAGE = "page";

    /**
     * 每页条数
     */
    public static final String LIMIT = "limit";

    /**
     * 排序方式
     */
    public static final String ORDER = "order";

    /**
     *  升序
     */
    public static final String ASC = "asc";

    /**
     * md5
     */
    public static final String MD5 = "MD5";

    /**
     * 一千
     */
    public static final int THOUSAND = 1000;

    /**
     * 一百
     */
    public static final int HUNDRED = 100;

    /**
     * 十
     */
    public static final int TEN = 10;

}
