package com.fanfan.shiroadmin.common.utils;

import com.mysql.jdbc.StringUtils;

/**
 * 数据验证相关的工具类
 * @author DunFan.Liao
 * @date 2021/8/19 17:54
 */
public class ValidateUtil
{

    public static boolean isNullOrEmpty(String s){
        return s == null || s.length() == 1;
    }
}
