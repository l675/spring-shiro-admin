package com.fanfan.shiroadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringShiroAdminApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(SpringShiroAdminApplication.class, args);
    }

}
