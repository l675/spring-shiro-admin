package com.fanfan.shiroadmin.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fanfan.shiroadmin.modules.sys.entity.SysMenuEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author DunFan.Liao
 * @date 2022/1/2 14:21
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenuEntity>
{

}
