package com.fanfan.shiroadmin.modules.sys.constant;

/**
 * 系统模块常量配置
 * @author DunFan.Liao
 * @date 2021/12/27 15:38
 */
public class SysConstant
{
    /**
     * 锁定状态
     */
    public static final String LOCK_STATE = "0";

    public static final String HEX_VALUE = "0123456789abcdef";

    public static final Long ADMIN_USER_ID = 1L;


}
