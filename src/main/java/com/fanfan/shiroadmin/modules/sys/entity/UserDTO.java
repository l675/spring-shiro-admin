package com.fanfan.shiroadmin.modules.sys.entity;

import java.util.Date;

/**
 * @author DunFan.Liao
 * @date 2021/9/4 15:10
 */
public class UserDTO
{
    private String userName;
    private Integer age;
    private Date date;

    public UserDTO()
    {

    }
    public UserDTO(String userName, Integer age, Date date)
    {
        this.userName = userName;
        this.age = age;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "userName='" + userName + '\'' +
                ", age=" + age +
                ", date=" + date +
                '}';
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
