package com.fanfan.shiroadmin.modules.sys.auth;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author DunFan.Liao
 * @date 2021/12/28 19:37
 */
public class SysAuthToken implements AuthenticationToken
{

    private String token;

    public SysAuthToken(String token)
    {
        this.token = token;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    @Override
    public Object getPrincipal()
    {
        return token;
    }

    @Override
    public Object getCredentials()
    {
        return token;
    }
}
