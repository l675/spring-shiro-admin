package com.fanfan.shiroadmin.modules.sys.service.impl;

import com.fanfan.shiroadmin.common.exception.ServiceRuntimeException;
import com.fanfan.shiroadmin.modules.sys.dao.SysUserMapper;
import com.fanfan.shiroadmin.modules.sys.entity.SysUserEntity;
import com.fanfan.shiroadmin.modules.sys.service.ISysUserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户服务
 * @author DunFan.Liao
 * @date 2021/12/27 11:26
 */
@Service(ISysUserService.BEAN_ID)
@Log4j2
public class SysUserServiceImpl implements ISysUserService
{

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public int insert(SysUserEntity user) throws ServiceRuntimeException
    {
        return sysUserMapper.insertSelective(user);
    }

    @Override
    public int update(SysUserEntity user) throws ServiceRuntimeException
    {
        return sysUserMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public SysUserEntity queryById(Long id) throws ServiceRuntimeException
    {
        return sysUserMapper.selectByPrimaryKey(id);
    }

    @Override
    public SysUserEntity queryByOther(String userAcct, String userName, String userEmail, String phone) throws ServiceRuntimeException
    {
        return sysUserMapper.queryByOther(userAcct, userName, userEmail, phone);
    }
}
