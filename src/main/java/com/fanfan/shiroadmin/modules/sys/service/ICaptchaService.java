package com.fanfan.shiroadmin.modules.sys.service;

import java.awt.image.BufferedImage;

/**
 * 登陆验证码服务
 * @author DunFan.Liao
 * @date 2021/8/19 17:43
 */
public interface ICaptchaService
{
    /**
     * BEAN_ID
     */
    String BEAN_ID = "sys.captchaService";

    /**
    * 功能描述：获得图片验证码
    * @param uuid 前端定义的uuid
    * @return 图片数据
    * @author DunFan.Liao
    * @date 2021/8/19 17:46
    */
    BufferedImage getCaptcha(String uuid);

    /**
    * 功能描述：验证uuid是否有效
    * @param uuid 前端定义的uuid
    * @param code 验证码
    * @return ture 有效 false 无效
    * @author DunFan.Liao
    * @date 2021/8/19 18:58
    */
    boolean validateCaptcha(String uuid,String code);

}
