package com.fanfan.shiroadmin.modules.sys.controller;

import com.alibaba.fastjson.JSONObject;
import com.fanfan.shiroadmin.common.web.controller.AbstractController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author DunFan.Liao
 * @date 2021/12/25 22:03
 */
@RestController
@RequestMapping("sys/user")
public class SysUserController extends AbstractController
{

    @GetMapping("/info")
    public JSONObject getUserInfo ()
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userInfo", getUser());
        return jsonObject;
    }

}
