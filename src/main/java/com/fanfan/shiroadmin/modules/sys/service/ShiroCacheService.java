package com.fanfan.shiroadmin.modules.sys.service;

import com.fanfan.shiroadmin.common.exception.ServiceRuntimeException;
import com.fanfan.shiroadmin.modules.sys.entity.SysMenuEntity;
import com.fanfan.shiroadmin.modules.sys.entity.SysUserEntity;

import java.util.List;
import java.util.Set;

/**
 * shiro cache service
 * @author DunFan.Liao
 * @date 2022/1/2 9:39
 */
public interface ShiroCacheService
{
    String BEAN_ID = "sys.shiroCacheService";

    /**
     * 功能描述：设置用户菜单缓存信息
     * @param user 登陆用户
     * @return 菜单缓存信息列表
     * @author DunFan.Liao
     * @date 2022/1/2 14:45
     * @throws ServiceRuntimeException 服务运行时异常
     */
    void addUserMenuCache(SysUserEntity user) throws ServiceRuntimeException;

    /**
     * 功能描述：设置用户资源缓存信息
     * @param user 登陆用户
     * @return 菜单缓存信息列表
     * @author DunFan.Liao
     * @date 2022/1/2 14:50
     * @throws ServiceRuntimeException 服务运行时异常
     */
    Set<String> addUserRsUrlCache(SysUserEntity user) throws ServiceRuntimeException;
}
