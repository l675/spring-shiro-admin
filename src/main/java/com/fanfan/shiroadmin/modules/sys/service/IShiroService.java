package com.fanfan.shiroadmin.modules.sys.service;

import com.alibaba.fastjson.JSONObject;
import com.fanfan.shiroadmin.common.exception.ServiceRuntimeException;
import com.fanfan.shiroadmin.modules.sys.entity.SysUserEntity;

/**
 *
 * @author DunFan.Liao
 * @date 2021/12/27 15:43
 */
public interface IShiroService
{
    /**
     * bean id
     */
    String BEAN_ID = "sys.shiroService";

    /**
     * 功能描述：获取token
     * @param user 用户信息
     * @return token
     * @author DunFan.Liao
     * @date 2021/12/27 15:45
     * @throws ServiceRuntimeException 服务运行时异常
     */
    JSONObject createToken(SysUserEntity user) throws ServiceRuntimeException;

    /**
     * 功能描述：根据token查询缓存中的登录信息
     * @param token 验证信息
     * @return 登录信息
     * @author DunFan.Liao
     * @date 2021/12/28 21:20
     * @throws ServiceRuntimeException 服务运行时异常
     */
    JSONObject queryUserByToken(String token) throws ServiceRuntimeException;

    /**
     * 功能描述：设置用户权限
     * @param userId 用户id
     * @return 权限集合
     * @author DunFan.Liao
     * @date 2021/12/31 9:40
     * @throws ServiceRuntimeException 服务运行时异常
     */
    JSONObject getStringPermissions(Long userId) throws ServiceRuntimeException;
}
