package com.fanfan.shiroadmin.modules.sys.service;

import com.fanfan.shiroadmin.common.exception.ServiceRuntimeException;
import com.fanfan.shiroadmin.modules.sys.entity.SysUserEntity;

/**
 * 用户信息接口
 * @author DunFan.Liao
 * @date 2021/12/27 11:12
 */
public interface ISysUserService
{
    /**
     * bean id
     */
    String BEAN_ID = "sys.userService";

    /**
     * 功能描述：保存单条用户信息
     * @param user 用户信息实体
     * @return 更新次数
     * @author DunFan.Liao
     * @date 2021/12/27 11:24
     * @throws ServiceRuntimeException 服务运行时异常
     */
    int insert(SysUserEntity user) throws ServiceRuntimeException;

    /**
     * 功能描述：更新单条用户信息
     * @param user 用户信息实体
     * @return 更新次数
     * @author DunFan.Liao
     * @date 2021/12/27 11:25
     * @throws ServiceRuntimeException 服务运行时异常
     */
    int update(SysUserEntity user) throws ServiceRuntimeException;

    /**
     * 功能描述：根据主键Id查询用户信息
     * @param id 主键id
     * @return 用户信息
     * @author DunFan.Liao
     * @date 2021/12/27 11:26
     * @throws ServiceRuntimeException 服务运行时异常
     */
    SysUserEntity queryById(Long id) throws ServiceRuntimeException;

    /**
     * 功能描述：根据用户其他信息查询
     * @param  userAcct 用户账号
     * @param userName 用户名称
     * @param userEmail 用户邮箱
     * @param phone 用户电话号码
     * @return 用户信息
     * @author DunFan.Liao
     * @date 2021/12/27 13:27
     * @throws ServiceRuntimeException 服务运行时异常
     */
    SysUserEntity queryByOther(String userAcct,String userName,String userEmail,String phone)throws ServiceRuntimeException;

}
