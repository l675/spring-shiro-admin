package com.fanfan.shiroadmin.modules.sys.controller;

import com.alibaba.fastjson.JSONObject;
import com.fanfan.shiroadmin.common.exception.ServiceRuntimeException;
import com.fanfan.shiroadmin.common.web.context.ResponseResultBody;
import com.fanfan.shiroadmin.common.web.controller.AbstractController;
import com.fanfan.shiroadmin.modules.sys.constant.SysConstant;
import com.fanfan.shiroadmin.modules.sys.entity.SysUserEntity;
import com.fanfan.shiroadmin.modules.sys.service.ICaptchaService;
import com.fanfan.shiroadmin.modules.sys.service.IShiroService;
import com.fanfan.shiroadmin.modules.sys.service.ISysUserService;
import com.fanfan.shiroadmin.modules.sys.dto.SysLoginDto;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * 登陆信息验证
 * @author DunFan.Liao
 * @date 2021/8/19 16:02
 */
@RestController
public class LoginController extends AbstractController
{

    @Resource(name = ICaptchaService.BEAN_ID)
    private ICaptchaService captchaService;

    @Resource(name = ISysUserService.BEAN_ID)
    private ISysUserService sysUserService;

    @Resource(name = IShiroService.BEAN_ID)
    private IShiroService shiroService;

    @GetMapping("captcha.jpg")
    public void captcha(HttpServletResponse response, String uuid)throws IOException
    {
        response.setHeader("Cache-Control","no-store, no-cache");
        response.setContentType("image/jpeg");
        BufferedImage captcha = captchaService.getCaptcha(uuid);
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(captcha, "jpg", out);
        IOUtils.closeQuietly(out);
    }

    @PostMapping("/sys/login")
    public JSONObject login(@Valid SysLoginDto loginDto, HttpServletRequest request)
    {
//        // 验证验证码是否正确
//        boolean captcha = captchaService.validateCaptcha(loginVo.getUuid(), loginVo.getCaptcha());
//        if (!captcha){
//            return ResponseResultBody.createFailedResult("验证码不正确");
//        }
        // 验证账号和密码
         SysUserEntity userEntity = sysUserService.queryByOther(loginDto.getUserAcct(),null,null,null);
         if (userEntity == null)
         {
             throw new ServiceRuntimeException("找不到该账号信息，请先进行注册");
         }
         if (!userEntity.getLoginPw().equals(new Sha256Hash(loginDto.getPassword(), userEntity.getPwSalt()).toHex()))
         {
             throw new ServiceRuntimeException("密码错误，请重新输入");
         }
         if (SysConstant.LOCK_STATE.equals(userEntity.getUserSts()))
         {
             throw new ServiceRuntimeException("账号已经被锁定，请联系管理员");
         }

         // 生成token
        JSONObject token = shiroService.createToken(userEntity);
        return ResponseResultBody.createSuccessResult(token);

    }

    public static void main(String[] args)
    {
        System.out.println(new Sha256Hash("admin","YzcmCZNvbXocrsz9dm8e").toHex());
        System.out.println(new Sha256Hash("9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d","YzcmCZNvbXocrsz9dm8e").toHex());
    }
}
