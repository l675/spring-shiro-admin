package com.fanfan.shiroadmin.modules.sys.entity;

import com.fanfan.shiroadmin.common.web.entiy.BaseLongEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * 系统资源实体
 * @author DunFan.Liao
 * @date 2021/12/25 20:07
 */
@ToString
@Setter
@Getter
public class SysRsEntity extends BaseLongEntity
{
    /**
     * 菜单id
     */
    private Long menuId;
    /**
     * 资源名称
     */
    private String rsName;
    /**
     * 资源路径
     */
    private String rsUrl;
    /**
     * 资源视图
     */
    private String rsView;
    /**
     * 资源样式
     */
    private String rsCls;
    /**
     * 资源顺序
     */
    private int rsOrd;
    /**
     * 资源类型
     */
    private String rsType;
    /**
     * 资源描述
     */
    private String rsDesc;
}
