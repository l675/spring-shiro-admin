package com.fanfan.shiroadmin.modules.sys.service;

import com.fanfan.shiroadmin.common.exception.ServiceRuntimeException;
import com.fanfan.shiroadmin.modules.sys.entity.SysMenuEntity;

import java.util.List;

/**
 * @author DunFan.Liao
 * @date 2022/1/2 13:58
 */
public interface ISysMenuService
{
    String BEAN_ID = "sys.menuService";

    /**
     * 功能描述：获取菜单列表（父子结构）
     * @return 菜单列表
     * @author DunFan.Liao
     * @date 2022/1/2 14:13
     * @throws ServiceRuntimeException 服务运行时异常
     */
    List<SysMenuEntity> queryMenuList() throws ServiceRuntimeException;
}
