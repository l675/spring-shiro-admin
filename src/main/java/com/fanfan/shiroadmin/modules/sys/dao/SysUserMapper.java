package com.fanfan.shiroadmin.modules.sys.dao;

import com.fanfan.shiroadmin.common.exception.ServiceRuntimeException;
import com.fanfan.shiroadmin.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 用户信息mapper
 * @author DunFan.Liao
 * @date 2021/12/27 11:15
 */
@Mapper
public interface SysUserMapper {

    /**
     * 功能描述：根据主键Id查询用户信息
     * @param id 主键id
     * @return 用户信息
     * @author DunFan.Liao
     * @date 2021/12/27 11:20
     * @throws ServiceRuntimeException 服务运行时异常
     */
    SysUserEntity selectByPrimaryKey(Long id) throws ServiceRuntimeException;

    /**
     * 功能描述：保存单条用户信息
     * @param entity 用户信息实体
     * @return 更新次数
     * @author DunFan.Liao
     * @date 2021/12/27 11:21
     * @throws ServiceRuntimeException 服务运行时异常
     */
    int insertSelective(SysUserEntity entity) throws ServiceRuntimeException;

    /**
     * 功能描述：更新单条用户信息
     * @param entity 用户信息实体
     * @return 更新次数
     * @author DunFan.Liao
     * @date 2021/12/27 11:21
     * @throws ServiceRuntimeException 服务运行时异常
     */
    int updateByPrimaryKeySelective(SysUserEntity entity) throws ServiceRuntimeException;

    /**
     * 功能描述：根据用户其他信息查询
     * @param  userAcct 用户账号
     * @param userName 用户名称
     * @param userEmail 用户邮箱
     * @param phone 用户电话号码
     * @return 用户信息
     * @author DunFan.Liao
     * @date 2021/12/27 13:27
     * @throws ServiceRuntimeException 服务运行时异常
     */
    SysUserEntity queryByOther(@Param("userAcct") String userAcct, @Param("userName") String userName, @Param("userEmail") String userEmail, @Param("phone") String phone)throws ServiceRuntimeException;
}
