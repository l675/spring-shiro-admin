package com.fanfan.shiroadmin.modules.sys.service.impl;

import com.fanfan.shiroadmin.common.exception.ServiceRuntimeException;
import com.fanfan.shiroadmin.modules.sys.dao.SysRsMapper;
import com.fanfan.shiroadmin.modules.sys.service.ISysRsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * @author DunFan.Liao
 * @date 2022/1/2 16:09
 */
@Service(ISysRsService.BEAN_ID)
public class SysRsServiceImpl implements ISysRsService
{

    @Autowired
    private SysRsMapper sysRsMapper;

    @Override
    public Set<String> queryRsUrlList() throws ServiceRuntimeException
    {
        return sysRsMapper.queryRsUrl();
    }
}
