package com.fanfan.shiroadmin.modules.sys.entity;

import com.fanfan.shiroadmin.common.web.entiy.BaseLongEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 角色表实体对象
 * @author DunFan.Liao
 * @date 2021/12/25 19:59
 */
@Getter
@Setter
@ToString
public class SysRoleEntity extends BaseLongEntity
{
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色描述
     */
    private String roleDesc;
    /**
     * 角色类型：100:ADMIN角色,200:子管理员角色,400:普通角色
     */
    private String roleType;
    /**
     * 是否内置角色 1内置角色 0非内置角色
     */
    private String roleFlag;
}
