package com.fanfan.shiroadmin.modules.sys.entity;

import com.fanfan.shiroadmin.common.web.entiy.ISubjectUser;
import com.fanfan.shiroadmin.common.web.entiy.SubjectUser;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 功能描述：用户实体
 * @author DunFan.Liao
 * @date 2021/9/29 14:11
 */
public class SysUserEntity  extends SubjectUser implements ISubjectUser
{
    /** 超级管理员 */
    private final Map<String, Boolean> superAdmins = new LinkedHashMap<String, Boolean>();
    /** 拼音 */
    private String userPinyin;

    /** 登陆密码 */
    private String loginPw;

    /** 密码盐 */
    private String pwSalt;

    /** 用户年龄 */
    private Integer userAge;

    /** 用户性别 */
    private String userGen;

    /** 1 正常 0 锁定 */
    private String userSts;

    /** 电话号码 */
    private String phoneSms;


    /** 用户地址 */
    private String userAddr;

    /** 用户邮箱 */
    private String userEmail;

    public String getPhoneSms()
    {
        return phoneSms;
    }

    public void setPhoneSms(String phoneSms)
    {
        this.phoneSms = phoneSms;
    }

    public String getUserPinyin()
    {
        return userPinyin;
    }

    public void setUserPinyin(String userPinyin)
    {
        this.userPinyin = userPinyin == null ? null : userPinyin.trim();
    }

    public String getLoginPw()
    {
        return loginPw;
    }

    public void setLoginPw(String loginPw)
    {
        this.loginPw = loginPw == null ? null : loginPw.trim();
    }

    public String getPwSalt()
    {
        return pwSalt;
    }

    public void setPwSalt(String pwSalt)
    {
        this.pwSalt = pwSalt == null ? null : pwSalt.trim();
    }

    public Integer getUserAge()
    {
        return userAge;
    }

    public void setUserAge(Integer userAge)
    {
        this.userAge = userAge;
    }

    public String getUserGen()
    {
        return userGen;
    }

    public void setUserGen(String userGen)
    {
        this.userGen = userGen == null ? null : userGen.trim();
    }

    public String getUserSts()
    {
        return userSts;
    }

    public void setUserSts(String userSts)
    {
        this.userSts = userSts == null ? null : userSts.trim();
    }

    public String getUserAddr()
    {
        return userAddr;
    }

    public void setUserAddr(String userAddr)
    {
        this.userAddr = userAddr == null ? null : userAddr.trim();
    }

    public String getUserEmail()
    {
        return userEmail;
    }

    public void setUserEmail(String userEmail)
    {
        this.userEmail = userEmail == null ? null : userEmail.trim();
    }

    @Override
    public Long getLoginUserId()
    {
        return this.getId();
    }

    @Override
    public String getLoginUserAccount()
    {
        return this.getUserAcct();
    }

    @Override
    public String getLoginUserName()
    {
        return this.getUserName();
    }

    /**
     * 获得当前登录用户是否为超级管理员
     *
     * @return 当前登录用户是超级管理员返回true, 否则false
     */
    @Override
    public boolean isSuperAdmin()
    {
        return superAdmins.getOrDefault(getCurrentAppCode(),false);
    }

    @Override
    public String getCurrentAppCode()
    {
        return this.getAppCode();
    }

    public void setCurrentAppCode(String appCode)
    {
        this.setAppCode(appCode);
    }

    public Map<String,Boolean> getSuperAdmins()
    {
        Map<String,Boolean> results = new LinkedHashMap<>();
        for (Map.Entry<String, Boolean> next : this.superAdmins.entrySet()) {
            results.put(next.getKey(), next.getValue());
        }
        return Collections.unmodifiableMap(results);
    }

    public void setSuperAdmins(Map<String, Boolean> superAdmins)
    {
        for (Map.Entry<String, Boolean> next : superAdmins.entrySet()) {
            this.superAdmins.put(next.getKey(), next.getValue());
        }
    }

}
