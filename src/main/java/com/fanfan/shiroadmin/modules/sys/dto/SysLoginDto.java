/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.fanfan.shiroadmin.modules.sys.dto;

import lombok.Data;


/**
 * 登录表单
 *
 * @author DunFan.Liao
 * @date 2021-08-19 19:31
 */
@Data
public class SysLoginDto
{

    //@NotBlank(message = "账号不能为空")
    private String userAcct;
    //@NotBlank(message = "密码不能为空，且不小于5位")
    private String password;
    private String captcha;
    // @NotBlank(message = "uuid不能为空")
    private String uuid;

}
