package com.fanfan.shiroadmin.modules.sys.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统权限实体
 * @author DunFan.Liao
 * @date 2021/12/25 20:14
 */
@Getter
@Setter
@ToString
public class SysAuthEntity implements Serializable
{
    /**
     * 控制者ID
     */
    private Long authId;
    /**
     * 控制者类型 用户:100,角色:200,岗位:400,菜单:300,资源:500
     */
    private String authType;
    /**
     * 控制对象ID
     */
    private Long authObjId;
    /**
     * 控制对象类型
     */
    private String authObjType;
    /**
     * 创建者ID
     */
    private Long createdBy;
    /**
     * 创建时间
     */
    private Date createdDate;
}
