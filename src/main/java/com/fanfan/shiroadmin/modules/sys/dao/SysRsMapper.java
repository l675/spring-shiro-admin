package com.fanfan.shiroadmin.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fanfan.shiroadmin.common.exception.ServiceRuntimeException;
import com.fanfan.shiroadmin.modules.sys.entity.SysRsEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.Set;

/**
 * @author DunFan.Liao
 * @date 2022/1/2 15:51
 */
@Mapper
public interface SysRsMapper extends BaseMapper<SysRsEntity>
{
    Set<String> queryRsUrl() throws ServiceRuntimeException;
}
