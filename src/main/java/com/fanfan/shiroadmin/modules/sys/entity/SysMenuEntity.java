package com.fanfan.shiroadmin.modules.sys.entity;

import com.fanfan.shiroadmin.common.web.entiy.BaseLongEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author DunFan.Liao
 * @date 2022/1/2 14:02
 */
@Getter
@Setter
@ToString
public class SysMenuEntity extends BaseLongEntity
{
    /**
     * 父菜单ID
     */
    private Long parentId;
    /**
     * 菜单名称
     */
    private String menuName;
    /**
     * 菜单路径
     */
    private String menuUrl;
    /**
     * 菜单样式
     */
    private String menuCls;
    /**
     * 菜单顺序
     */
    private int menuOrd;
    /**
     * 菜单类型
     */
    private String menuType;
    /**
     * 菜单级别
     */
    private int menuLvl;
    /**
     * 菜单描述
     */
    private String menuDesc;

    /**
     * 孩子节点
     */
    private List<SysMenuEntity> child;
}
