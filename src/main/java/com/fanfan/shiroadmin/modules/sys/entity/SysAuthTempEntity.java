package com.fanfan.shiroadmin.modules.sys.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 权限临时表
 * @author DunFan.Liao
 * @date 2021/12/25 20:20
 */
@ToString
@Setter
@Getter
public class SysAuthTempEntity implements Serializable
{
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 用户类型
     */
    private String userType;
    /**
     * 控制者ID
     */
    private Long authId;
    /**
     * 控制者类型
     */
    private String authType;
    /**
     * 角色ID
     */
    private Long roleId;
    /**
     * 角色类型
     */
    private String roleType;
    /**
     * 资源ID
     */
    private Long rsId;
    /**
     * 资源类型
     */
    private String rsType;
    /**
     * 创建者ID
     */
    private Long createdBy;
    /**
     * 创建时间
     */
    private Date createdDate;
}
