package com.fanfan.shiroadmin.modules.sys.service;

import com.fanfan.shiroadmin.common.exception.ServiceRuntimeException;

import java.util.Set;

/**
 * @author DunFan.Liao
 * @date 2022/1/2 16:06
 */
public interface ISysRsService
{
    String BEAN_ID = "sys.rsService";

    /**
     * 功能描述：获取资源路径列表
     * @return 资源路径列表
     * @author DunFan.Liao
     * @date 2022/1/2 16:08
     * @throws ServiceRuntimeException 服务运行时异常
     */
    Set<String> queryRsUrlList() throws ServiceRuntimeException;
}
