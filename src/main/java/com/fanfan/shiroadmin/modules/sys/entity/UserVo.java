package com.fanfan.shiroadmin.modules.sys.entity;

/**
 * @author DunFan.Liao
 * @date 2021/9/4 14:50
 */

public class UserVo
{
    private String userName;
    private int age;

    public UserVo(String userName, int age)
    {
        this.userName = userName;
        this.age = age;
    }

    @Override
    public String toString()
    {
        return "UserVo{" +
                "userName='" + userName + '\'' +
                ", age=" + age +
                '}';
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }
}
